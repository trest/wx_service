var express = require('express');
var router = express.Router();
var wx = require('./../api/wx');
/* GET wx listing. */
router.get('/wx/:name', function(req, res, next) {
  var api_name=req.params.name;
  wx[api_name].main(req, res, next);
});
module.exports = router;
