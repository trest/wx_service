/**
 * Created by qs on 2015/3/20.
 */

/*
微信接收信息模块
 */

var config=require('./../config/config');
var crypto=require('crypto');
exports.wx_acceptor={
    main:function(req, res, next){
        //res.send(req.query.echostr);
        if(this.validation(req)){
            res.send(req.query.echostr);
        }else{
            var error =new Error(500);
            next(error);
        }

    },
    validation:function(req){
        var signature=req.query.signature.toString();
        var timestamp=req.query.timestamp.toString();
        var token=config.wx_config.wx_token.toString();
        var nonce=req.query.nonce.toString();
        var wx_array=new Array(token,timestamp,nonce);
        wx_array.sort();
        var sha1=crypto.createHash("sha1");
        for(var temp in wx_array){
            sha1.update(wx_array[temp]);
        }
        sha1_str=sha1.digest('hex');
        if(sha1_str==signature){
            return true;
        }else{
            return false;
        }
    }
};
exports.tool={
    array_implode:function(tmp_array){
        var tmp_str='';
        for(var tmp in tmp_array){
            tmp_str+=tmp_array[tmp];
        }
        return tmp_str;
    }
}





